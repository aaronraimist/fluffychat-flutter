# Version 0.5.0 - 2020-01-20
### New features
- Localizations
- Enhanced group settings
- Username search
### Fixes
- Minor bug fixes

# Version 0.4.0 - 2020-01-18
### New features
- Registration
- Avatars with name letters
- Calc username colors
### Fixes

# Version 0.3.0 - 2020-01-17
### New features
- Improved design
- Implement signing up (Still needs a matrix.org fix)
- Forward messages
- Content viewer
### Fixes
- Chat textfield isn't scrollable on large text
- Text disappears in textfield on resuming the app

# Version 0.2.3 - 2020-01-08
### New features
- Added changelog
- Added copy button
### Fixes
- Groups without name now have a "Group with" prefix
